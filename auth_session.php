<?php
    session_start();
    if(!isset($_SESSION["emailaddress"])) {
        header("Location: login.php");
        exit();
    }
?>
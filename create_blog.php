<?php
    session_start();
    require('database.php');
    include('auth_session.php');
    $email = $_SESSION['emailaddress'];

    $sql2 = "SELECT * FROM users WHERE e_mail = '$email'";
    $res2 = mysqli_query($con, $sql2);
    $row2 = mysqli_fetch_array($res2);
    $id = $row2['user_id'];
    $checkbox1 = $_POST['selcats'] ; 

    $sql1 = "SELECT * from categories";
    $res1 = mysqli_query($con, $sql1);

    if (isset($_POST['create_blog']))
    {
        
        if(empty($_POST["title"]) ||
                empty($_POST["content"])){
				$_SESSION["messages"][] = "Please Fill All the Credentials   Carefully.";
				header("location: create_blog.php");
				exit;
        }
        $file = $_FILES['image']['tmp_name'];
        if(!isset($file))
        {
            $_SESSION["messages"][] = "Please Select an image.";
            header("location: create_blog.php");
            exit;
        }
        $image_check = getimagesize($_FILES['image']['tmp_name']);
        if($image_check==false)
        {
            $_SESSION["messages"][] = "Not a valid Image.";
            header("location: create_blog.php");
            exit;
        }
        $image = addslashes(file_get_contents($_FILES['image']['tmp_name'])); //SQL Injection defence!
        $image_name = addslashes($_FILES['image']['name']);

        $title = $_POST["title"];
        $content = $_POST["content"];

        $sql = "INSERT INTO blogs (title, content, img, img_name, author_id) VALUES ('$title', '$content' , '$image' , '$image_name', '$id')";
        // print_r($sql);exit;
        if ($con->query($sql) === TRUE) {
            $last_id = $con->insert_id;
            
            $CheckBox=$_POST['selcats'];
            $checked = sizeof($CheckBox);

            foreach($CheckBox as $checked)
            {
                $queryVideo = mysqli_query($con,"INSERT INTO blogcat (blog_id,cat_name) VALUES ('$last_id','$checked');");
            }
            // for($i=0;$i<$checked;$i++) {
            //     $selcat = $CheckBox[$i];
            //     $queryVideo = mysqli_query($con,"INSERT INTO blogcat (blog_id,cat_name) VALUES ('$last_id','$selcat');");  
            // }
            
            // if($checked == 0) {
            //     echo'<script>alert("Failed To Insert")</script>';  
            // } elseif($queryVideo) {
            //     echo'<script>alert("Inserted Successfully")</script>';
            // }

            header("location: dashboard.php");
            exit;
            }
         else {
            $_SESSION["messages"][] = "Something gonna wrong";
            header("location: create_blog.php");
            exit;
            }
        
    };
    
?>
<html> 

    <head> 
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="./style1.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.23.0/ui/trumbowyg.min.css"
        integrity="sha512-iw/TO6rC/bRmSOiXlanoUCVdNrnJBCOufp2s3vhTPyP1Z0CtTSBNbEd5wIo8VJanpONGJSyPOZ5ZRjZ/ojmc7g=="
        crossorigin="anonymous"/>

        <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/ui-lightness/jquery-ui.css" rel='stylesheet'> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src= "https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" > </script> 
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.19.0/jquery.validate.min.js"></script>
        <!-- Bootstrap CSS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js" integrity="sha512-UdIMMlVx0HEynClOIFSyOrPggomfhBKJE28LKl8yR3ghkgugPnG6iLfRfHwushZl1MOPSY6TsuBDGPK2X4zYKg==" crossorigin="anonymous"></script>

        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <title>Create blog.</title> 
    </head> 

    <body>
    <div class="container-fluid">
            <div class="row justify-content-center text-center">
                <div class="col-lg-12">
                    <h1>Create New Blog:</h1>
                    <hr> 
                </div>
            </div>
            <div class="row justify-content-center text-center">
                <div class="col-lg-8 mt-3 mb-3">
                    <div class="card">
                        <div class="card-body">
                            <form  action="create_blog.php" id="form" method="POST" enctype="multipart/form-data"> 
                            <?php require_once 'messages.php'; ?>
                                <label for="title"><b>Set Title: </b></label>
                                <input type="text" id="title" name="title" /> 
                                <br> 
                                <br> 
                                <label for="content"><b>Enter Content: </b></label>
                                <textarea id="content" name="content">
                                </textarea>
                                <br>
                                <br>
                                <label for="img"><b>Select Image: </b></label>
                                <input type="file" id="image" name="image" >
                                <br> 
                                <br>
                                <label for="pwd"><b>Select categories: </b></label>
                                <?php
                                    while($row1 = mysqli_fetch_assoc($res1)){
                                        echo ('<input type="checkbox" name="selcats[]" value="'.$row1['cat_name'].'"> ' .$row1['cat_name']);
                                    };
                                ?>
                                <br> 
                                <br>
                                <a class="btn btn-dark" href="dashboard.php" role="button">Return Back</a>

                                <input class="mt-3 mb-3 btn btn-dark" type="submit" id="create_blog" value="Create Blog" name="create_blog"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.23.0/trumbowyg.min.js"
        integrity="sha512-sffB9/tXFFTwradcJHhojkhmrCj0hWeaz8M05Aaap5/vlYBfLx5Y7woKi6y0NrqVNgben6OIANTGGlojPTQGEw=="
        crossorigin="anonymous"></script>

        <script src="./validation.js"></script>


        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script> 
    </body>
</html>



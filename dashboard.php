<?php
    require('database.php');
    include('auth_session.php');
    $email = $_SESSION['emailaddress'];
    $user = "SELECT * FROM users WHERE e_mail = '$email'";
    $users = mysqli_query($con, $user);
    
    $common = "SELECT b.author_id ,b.blog_id, b.title ,b.img, b.img_name, b.content, u.fname, u.lname, b.created_at FROM blogs as b INNER JOIN users u ON b.author_id = u.user_id";
    
    if(isset($_POST['but_search'])){
        $fromDate = $_POST['fromDate'];
        $endDate = $_POST['endDate'];

        if(!empty($fromDate) && !empty($endDate)){
            $sql1 = "$common WHERE b.created_at between '" . $fromDate. "' and '".$endDate."' ORDER BY b.blog_id DESC ";
            $res = mysqli_query($con, $sql1);
        }else{
            $errormsg = "<h2> Please Select Dates. </h2>";
        }
    }else{
    $sql1 = "$common ORDER BY b.blog_id DESC";
    $res = mysqli_query($con, $sql1);}
    $row2 = mysqli_fetch_assoc($users);
    $u_id = $row2["user_id"];
    if (isset($_POST['edit']))
    {   
        // echo $blogid;exit;
        // print_r($_POST);exit;
        if(empty($_POST["title"]) ||
                empty($_POST["content"])){
                $_SESSION["messages"][] = "Please Fill All the Credentials   Carefully.";
                header("location: edit.php?id='".$blogid."'");
                exit;
        }
        $id = $_POST["custId"];
        $title = $_POST["title"];
        // echo $id;exit;
        $content = $_POST["content"];

        $sql = "UPDATE blogs SET title = '$title', content = '$content' WHERE blog_id = '$id'";
        // print_r($sql);exit;
        if (mysqli_query($con, $sql)) {
            $_SESSION["messages"][] = "Data Updated Successfully.";
                    header("location: dashboard.php");
                    exit;
          } else {
            $_SESSION["messages"][] = "Something gona wrong.";
            header("location: edit.php?id='".$blogid."'");
            exit;
        }
    }
    
?>
<html>
    <head> 
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="./style1.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.23.0/ui/trumbowyg.min.css"
        integrity="sha512-iw/TO6rC/bRmSOiXlanoUCVdNrnJBCOufp2s3vhTPyP1Z0CtTSBNbEd5wIo8VJanpONGJSyPOZ5ZRjZ/ojmc7g=="
        crossorigin="anonymous"/>
        <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/ui-lightness/jquery-ui.css" rel='stylesheet'> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src= "https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" > </script> 
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.19.0/jquery.validate.min.js"></script>
        <!-- Bootstrap CSS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js" integrity="sha512-UdIMMlVx0HEynClOIFSyOrPggomfhBKJE28LKl8yR3ghkgugPnG6iLfRfHwushZl1MOPSY6TsuBDGPK2X4zYKg==" crossorigin="anonymous"></script>

        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <title>Blogs dashboard</title> 
    </head>
    <body>
        <div class="container">
            <div class="row  mt-5 mb-3">
                <div class="col-lg-12">
                    <div class="d-flex flex-column">
                        <div class="d-flex justify-content-center text-center">
                            <?php
                                echo "<h2> Welcome  " . $row2["fname"]. " " . $row2["lname"] ."</h2>";
                            ?>
                        </div>
                        <div class="d-flex mb-3 flex-sm-row flex-column justify-conent-center text-center align-items-center">
                            <div class="text-center mr-sm-auto">
                                <h3>All Blogs:</h3>
                            </div>
                            
                            <div class="pr-3 mt-3">
                                <a class="btn btn-dark" href="create_blog.php" role="button">Create New Blog</a>
                            </div>
                            <div class="mt-3">
                                <a class="btn btn-dark" href="logout.php" role="button">Logout</a>
                            </div>
                        </div>
                        <div>
                        <div class= "d-flex justify-content-end">
                            <div class="pr-3">
                                <h4> Filter: </h4>
                            </div>
                            <div>
                            <form method='post' action='dashboard.php'>
                                From <input type='text' class='dateFilter' name='fromDate' value='<?php if(isset($_POST['fromDate'])) echo $_POST['fromDate']; ?>'>
        
                                To <input type='text' class='dateFilter' name='endDate' value='<?php if(isset($_POST['endDate'])) echo $_POST['endDate']; ?>'>
                                
                                <input class="btn btn-sm btn-light" type="submit" id="search" value="Search" name="but_search"/>
                            </form>    
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <?php require_once 'messages.php'; ?>
            <div class="row mt-3 mb-3">

<?php
    echo $errormsg;
    if(mysqli_num_rows($res) > 0){
        while($row = mysqli_fetch_assoc($res)){
            
            $q = "SELECT bc.cat_name from blogcat as bc 
                INNER JOIN blogs as b ON b.blog_id = bc.blog_id
                where bc.blog_id = " . $row['blog_id'];
            $res1 = mysqli_query($con, $q);
            $row1 = mysqli_fetch_all($res1,MYSQLI_ASSOC);
            foreach($row as $r){
                    $row['cat'] = $row1;
            }
            echo ('
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <div class="card mt-3 mb-3">
                            <img src="data:image/jpg;charset=utf8;base64,'.base64_encode($row['img']).'" class="card-img-top img-fluid">
                            <div class="card-block">
                                <h3 class="card-title">Title: '.$row["title"].'</h3>
                                <h4>Content:</h4> <p class="text-muted"> '.$row["content"].'</p>
                                <h4>Cetogories:</h4> <p class="text-muted"> ');
                                $tmp = '';
                                foreach($row['cat'] as $c){
                                    $tmp .= $c['cat_name'] . ','; 
                                }
                                $tmp = trim($tmp, ','); 
                                echo $tmp;
                                echo('</p>');
                                if($row["author_id"] == $u_id){
                                   echo('<a href="edit.php?id='.$row["blog_id"].'" class="btn btn-dark">Edit</a>');
                                }else{
                                    echo('<a href="edit.php?id='.$row["blog_id"].'" class="btn btn-dark disabled" tabindex="-1">Edit</a>');
                                }
                                echo('<blockquote class="blockquote mb-0 text-right">
                                <footer class="blockquote-footer">Author: '.$row["fname"].' '.$row["lname"].'
                                </footer>
                                <footer class="blockquote-footer">Published At: '.$row["created_at"].'
                                </footer>
                                </blockquote>
                            </div>
                        </div>
                    </div> ');
        };
        
    }
    else{
        echo "<h2>No Records Founds between given dates.</h2>";
    }
?>
            </div>
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.23.0/trumbowyg.min.js"
        integrity="sha512-sffB9/tXFFTwradcJHhojkhmrCj0hWeaz8M05Aaap5/vlYBfLx5Y7woKi6y0NrqVNgben6OIANTGGlojPTQGEw=="
        crossorigin="anonymous"></script>

        <script src="./validation.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script> 
    </body> 
</html>
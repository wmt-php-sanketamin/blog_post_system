<?php
    // error_reporting(E_ALL);
    require('database.php');

    $blogid = $_GET['id'];
    $sql = "SELECT * FROM blogs WHERE blog_id = $blogid";
    $res = mysqli_query($con, $sql);
    $row = mysqli_fetch_assoc($res);
    $id = $row['blog_id'];
    // echo ($row['content']);
   
?>
<html> 

<head> 
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="./style1.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.23.0/ui/trumbowyg.min.css"
    integrity="sha512-iw/TO6rC/bRmSOiXlanoUCVdNrnJBCOufp2s3vhTPyP1Z0CtTSBNbEd5wIo8VJanpONGJSyPOZ5ZRjZ/ojmc7g=="
    crossorigin="anonymous"/>

    <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/ui-lightness/jquery-ui.css" rel='stylesheet'> 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src= "https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" > </script> 
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.19.0/jquery.validate.min.js"></script>
    <!-- Bootstrap CSS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js" integrity="sha512-UdIMMlVx0HEynClOIFSyOrPggomfhBKJE28LKl8yR3ghkgugPnG6iLfRfHwushZl1MOPSY6TsuBDGPK2X4zYKg==" crossorigin="anonymous"></script>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <title>Create blog.</title> 
</head> 

<body>
<div class="container-fluid">
        <div class="row justify-content-center text-center">
            <div class="col-lg-12">
                <h1>Edit Details:</h1>
                <hr> 
            </div>
        </div>
        <div class="row justify-content-center text-center">
            <div class="col-lg-8 mt-3 mb-3">
                <div class="card">
                    <div class="card-body">
                        <form  action="dashboard.php" id="form" method="POST" enctype="multipart/form-data"> 
                        <?php require_once 'messages.php'; ?>
                            <input type="hidden" id="custId" name="custId" value="<?php echo $row['blog_id'];?>">
                            <label for="title"><b>Set Title: </b></label>
                            <input type="text" id="title" value="<?php echo $row['title'];?>" name="title" /> 
                            <br> 
                            <br> 
                            <label for="content"><b>Enter Content: </b></label>
                            <textarea id="content" name="content"><?php echo ($row['content']);?>
                            </textarea>
                            <br>
                            <br>
                            <a class="btn btn-dark" href="dashboard.php" role="button">Return Back</a>

                            <input class="mt-3 mb-3 btn btn-dark" type="submit" id="create_blog" value="Update" name="edit"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.23.0/trumbowyg.min.js"
    integrity="sha512-sffB9/tXFFTwradcJHhojkhmrCj0hWeaz8M05Aaap5/vlYBfLx5Y7woKi6y0NrqVNgben6OIANTGGlojPTQGEw=="
    crossorigin="anonymous"></script>

    <script src="./validation.js"></script>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script> 
</body>
</html>

<?php
     
?>
<html>
    <head>
        <head> 
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="./style1.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <title>Login</title> 
        </head> 
    </head>
    <body>
		<div class="container-fluid">
			<div class="row justify-content-center text-center">
				<div class="col-lg-12">
					<h1>User Login Page:</h1>
					<hr> 
				</div>
			</div>
            <div class="row justify-content-center text-center">
            
            <div class="col-lg-8 mt-3 mb-3">
				<div class="card">  <!-- style="border: 0cm;-->
					<div class="card-body">
						<form action="sign_in.php" id="form2" method="POST" >
						<?php require_once 'messages.php'; ?>
                        <label for="e_add"><b>Email Address: </b></label>
							<input type="email" id="e_add" placeholder="Enter registered Email ID" name="emailaddress"/> 
							<br> 
							<br>
							<label for="pwd"><b>Password: </b></label>
							<input type="password" id="pwd" placeholder="Enter registered Password" name="password" />
							<br> 
                            <br>
                            <p class="link">Don't have an Account ? <a href="register.php">Register Now</a></p>
                            <input class="mt-3 mb-3 btn btn-dark" type="submit" id="loginbtn" value="Login" name="submit"/>
						</form>
					</div>
                </div>  
            </div>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>     
        </body>
</html>

$('#content').trumbowyg();

$().ready(function() {
    $(function() { 
        $(".dateFilter").datepicker({
            dateFormat: "yy-mm-dd",
            changeMonth: true,
            changeYear: true,
            yearRange: "1930:2021" 
        });
        $("#my_date_picker").datepicker({ 
            showButtonPanel: true,
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            yearRange: "1930:2021" 
        });
    }); 
    $("#form").validate({
        rules : {
            firstname : {
                required : true
            },
            lastname : {
                required : true
            },
            dob :{
                required: true,
                dpDate: true
            },
            email : {
                required : true,
                email : true
            },
            password : {
                required : true,
                minlength : 8,
            }
        },

        messages: {
            firstname: "Firstname is Required.",

            lastname: "Lastname is Required.",
            dob: {
                required: " * required: You must enter a destruction date",
                date: "Can contain digits only"
            }, 
            emailaddress: {
                required : "Email is required.",
                email : "Ente valid Email."
            }, 
            password: {
                required: "Enter your password",
                minlength: "Minimum password length is 8"
            },
        },

        submitHandler: function(form) {
            form.submit();
        }
    }); 
});